#/bin/bash
echo "========== Configure SSH =========="
mkdir -p ~/.ssh
chmod 700 ~/.ssh
cp .dockerfiles/authentication/id_rsa* ~/.ssh
chmod 600 ~/.ssh/id_rsa
chmod 644 ~/.ssh/id_rsa.pub
echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config
echo "SSH configuration done !!!"