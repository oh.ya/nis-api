#/bin/bash

sleep 60

echo "NISDOMAIN=$1" >> /etc/sysconfig/network
authconfig --enablenis --nisdomain=$1 --nisserver=$2 --enablemkhomedir --update
systemctl start rpcbind ypbind