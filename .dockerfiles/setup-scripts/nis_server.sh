#/bin/bash

sleep 30

ypdomainname $1
echo "NISDOMAIN=$1" >> /etc/sysconfig/network
echo "YPSERV_ARGS=\"-p 1011\"" >> /etc/sysconfig/network
systemctl start rpcbind ypserv ypxfrd yppasswdd
echo ^D | /usr/lib64/yp/ypinit -m