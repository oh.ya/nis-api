from flask import Flask
import subprocess

app = Flask(__name__)

@app.route("/createuser")
def create_user():
    inventory = "ansible/inventory"

    playbook1 = "ansible/playbooks/user_create.yaml"
    ansible_cmd1 = ['ansible-playbook', playbook1, '-i', inventory]
    result1 = subprocess.run(ansible_cmd1, capture_output=True, text=True)
    print(result1.stdout)
    print(result1.stderr)

    playbook2 = "ansible/playbooks/nis_make_database.yaml"
    ansible_cmd2 = ['ansible-playbook', playbook2, '-i', inventory]
    result2 = subprocess.run(ansible_cmd2, capture_output=True, text=True)
    print(result2.stdout)
    print(result2.stderr)

    return "<p>create user successfully</p>"


@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

# https://flask.palletsprojects.com/en/2.3.x/quickstart/